#################################################################################################################
############################################ INSTRUCTIONS #######################################################
#################################################################################################################

Ce projet utilise des algorithmes de Machine Learning pour prévoir la perte de client par les banques. 

Le dossier est organisé de la manière suivante. 
"data" contient les données brutes .
"intermediate" contient les tables transformées utilisées pour faire mes analyses. 
"output" contient les résultats finaux. 
"scripts" contient les codes qui ont servi à produire l'ensemble des éléments présents dans data, intermediate et output.

